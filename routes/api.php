<?php

use App\Http\Controllers\Api\Authentication\CustomerLogoutController;
use App\Http\Controllers\Api\Authentication\CustomerMeController;
use App\Http\Controllers\Api\Authentication\CustomerRegisterController;
use App\Http\Controllers\Api\Authentication\CustomerLoginController;
use App\Http\Controllers\Api\Authentication\UserLoginController;
use App\Http\Controllers\Api\Authentication\UserLogoutController;
use App\Http\Controllers\Api\Authentication\UserMeController;
use App\Http\Controllers\Api\Authentication\UserRegisterController;
use App\Http\Controllers\Api\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/auth/register')->group(function () {
    Route::post('user', UserRegisterController::class);
    Route::post('user/login', UserLoginController::class);
});

Route::prefix('/auth')->middleware('auth:sanctum')->group(function() {
    Route::post('user/logout', UserLogoutController::class);
    Route::post('user/me', UserMeController::class);
});

Route::prefix('/auth/register')->group(function () {
    Route::post('customer', CustomerRegisterController::class);
    Route::post('customer/login', CustomerLoginController::class);
});

Route::prefix('/auth')->middleware(['auth:sanctum', 'auth.customer'])->group(function() {
    Route::post('customer/logout', CustomerLogoutController::class);
    Route::post('customer/me', CustomerMeController::class);
});

Route::prefix('/product')->group(function () {
    Route::get('/', [ProductController::class, 'index'])->name('product.index');
    Route::get('/{product}', [ProductController::class, 'show'])->name('product.show');

    Route::middleware('auth:sanctum')->group(function () {
        Route::post('/', [ProductController::class, 'store'])->name('product.store');
        Route::put('/{product}', [ProductController::class, 'update'])->name('product.update');
        Route::delete('/{product}', [ProductController::class, 'destroy'])->name('product.destroy');
    });
});

