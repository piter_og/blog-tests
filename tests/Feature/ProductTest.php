<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use DatabaseMigrations;

    private $mainRoute = 'product';

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_can_api_create_product()
    {
        $user = $this->prepareAuthentication();

        $product = Product::factory()->make();

        $response = $this->postJson(route($this->mainRoute.'.store'), $product->toArray());

        $response
            ->assertStatus(201)
            ->assertJson([
                'success' => true,
            ]);

        $this->assertDatabaseHas('products', [
            'title' => $product->title,
            'description' => $product->description,
            'price' => $product->price,
            'quantity' => $product->quantity,
            'user_id' => $user->id
        ]);
    }

    public function test_cannot_create_product_user_not_authenticated()
    {
        $product = Product::factory()->make();

        $response = $this->postJson(route($this->mainRoute.'.store'), $product->toArray());

        $response
            ->assertStatus(401)
            ->assertJson([
                'success' => false,
            ]);
    }

    public function test_create_product_invalid_data()
    {
        $this->prepareAuthentication();

        $response = $this->postJson(route($this->mainRoute.'.store'), []);

        $response
            ->assertStatus(422)
            ->assertJson([
                'success' => false,
            ])
            ->assertJsonStructure([
                'success',
                'message',
                'data' => [
                    'title', 'quantity', 'price'
                ]
            ]);
    }

    public function test_can_update_self_product()
    {
        $user = $this->prepareAuthentication();

        $payload = Product::factory(['user_id' => $user->id])->definedUser()->create();

        $newProduct = [
            'title' => 'Test Title',
            'description' => 'Test Description',
            'price' => 999.99,
            'quantity' => 123
        ];

        $response = $this->putJson(route($this->mainRoute.'.update', $payload), [
            'title' => $newProduct['title'],
            'description' => $newProduct['description'],
            'price' => $newProduct['price'],
            'quantity' => $newProduct['quantity'],
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->assertDatabaseHas('products', [
            'title' => $newProduct['title'],
            'description' => $newProduct['description'],
            'price' => $newProduct['price'],
            'quantity' => $newProduct['quantity'],
            'user_id' => $user->id
        ]);
    }

    public function test_cannot_update_other_user_product()
    {
        $this->prepareAuthentication();
        $anotherUser = User::factory()->create([
            'email' => 'test_user_2@mail.com'
        ]);;

        $payload = Product::factory(['user_id' => $anotherUser->id])->definedUser()->create();

        $newProduct = [
            'title' => 'Test Title',
            'description' => 'Test Description',
            'price' => 999.99,
            'quantity' => 123
        ];

        $response = $this->putJson(route($this->mainRoute.'.update', $payload), [
            'title' => $newProduct['title'],
            'description' => $newProduct['description'],
            'price' => $newProduct['price'],
            'quantity' => $newProduct['quantity'],
        ]);

        $response
            ->assertStatus(401)
            ->assertJson([
                'success' => false,
            ]);

        $this->assertDatabaseHas('products', [
            'title' => $payload['title'],
            'description' => $payload['description'],
            'price' => $payload['price'],
            'quantity' => $payload['quantity'],
            'user_id' => $anotherUser->id
        ]);
    }

    public function test_cannot_update_product()
    {
        $user = $this->prepareAuthentication();

        Product::factory(['user_id' => $user->id])->definedUser()->create();

        $newProduct = [
            'title' => 'Test Title',
            'description' => 'Test Description',
            'price' => 999.99,
            'quantity' => 123
        ];

        $response = $this->putJson(route($this->mainRoute.'.update', 4), [
            'title' => $newProduct['title'],
            'description' => $newProduct['description'],
            'price' => $newProduct['price'],
            'quantity' => $newProduct['quantity'],
        ]);

        $response
            ->assertStatus(404)
            ->assertJson([
                'success' => false,
            ]);
    }

    public function test_can_delete_self_product()
    {
        $user = $this->prepareAuthentication();

        $product = Product::factory(['user_id' => $user->id])->definedUser()->create();

        $response = $this->deleteJson(route($this->mainRoute.'.destroy', $product->id));

        $response->assertStatus(204);
        $this->assertSoftDeleted('products', ['id' => $product->id]);
    }

    public function test_cannot_delete_other_user_product()
    {
        $user = $this->prepareAuthentication();

        $anotherUser = User::factory()->create([
            'email' => 'test_user_2@mail.com'
        ]);

        $payload = Product::factory(['user_id' => $anotherUser->id])->definedUser()->create();

        $response = $this->deleteJson(route($this->mainRoute.'.destroy', $payload->id));
        $response
            ->assertStatus(401)
            ->assertJson([
                'success' => false,
            ]);

        $this->assertDatabaseHas('products', [
            'title' => $payload['title'],
            'description' => $payload['description'],
            'price' => $payload['price'],
            'quantity' => $payload['quantity'],
            'user_id' => $anotherUser->id
        ]);
    }

    public function test_cannot_delete_product()
    {
        $user = $this->prepareAuthentication();

        Product::factory(['user_id' => $user->id])->definedUser()->create();

        $response = $this->deleteJson(route($this->mainRoute.'.destroy', 4));

        $response
            ->assertStatus(404)
            ->assertJson([
                'success' => false,
            ]);
    }

    private function prepareAuthentication()
    {
        $user = User::factory()->create([
            'email' => 'test@mail.com'
        ]);

        $this->actingAs($user);

        return $user;
    }
}
