<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\{UserRepository,CustomerRepository,DeliveryRepository,ProductRepository,OrderRepository,};
use App\Repositories\Contracts\{UserRepositoryInterface,CustomerRepositoryInterface,DeliveryRepositoryInterface,ProductRepositoryInterface,OrderRepositoryInterface,};

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(CustomerRepositoryInterface::class, CustomerRepository::class);
        $this->app->bind(DeliveryRepositoryInterface::class, DeliveryRepository::class);
        $this->app->bind(ProductRepositoryInterface::class, ProductRepository::class);
        $this->app->bind(OrderRepositoryInterface::class, OrderRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
