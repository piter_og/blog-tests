<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class CustomerRegisterRequest extends FormRequest
{
    public function prepareForValidation()
    {
        return $this->merge([
            'identity' => preg_replace('/\D/', '', $this->identity)
        ]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required','string'],
            'email' => ['required', 'email', 'unique:customers'],
            'password' => ['required', Password::min(8)->mixedCase()],
            'identity' => ['required', 'digits:11']
        ];
    }
}
