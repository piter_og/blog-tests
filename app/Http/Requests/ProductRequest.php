<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    public function prepareForValidation()
    {
        return $this->merge([
            'user_id' => auth()->user()->id
        ]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (in_array($this->method(), ['PUT', 'PATCH', 'DELETE'])) {
            $product = $this->route()->parameter('product');

            return $product->user_id == auth()->user()->id;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required'],
            'description' => ['string'],
            'price' => ['required', 'regex:/^\d{1,4}(\.\d{1,2})?$/'],
            'quantity' => ['required', 'numeric'],
            'user_id' => ['required', 'exists:users,id']
        ];
    }
}
