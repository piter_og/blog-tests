<?php

namespace App\Http\Controllers\Api\Authentication;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\CustomerRepositoryInterface;
use App\Traits\ApiResponserTrait;

class CustomerLogoutController extends Controller
{
    use ApiResponserTrait;

    /**
     * UserLogoutController constructor.
     * @param CustomerRepositoryInterface $mainRepository
     */
    public function __construct(
        private CustomerRepositoryInterface $mainRepository,
    ) {}

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        auth()->user()->tokens()->delete();

        return $this->success([], 'Tokens Revoked');
    }
}
