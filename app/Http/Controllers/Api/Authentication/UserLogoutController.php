<?php

namespace App\Http\Controllers\Api\Authentication;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Traits\ApiResponserTrait;

class UserLogoutController extends Controller
{
    use ApiResponserTrait;

    /**
     * UserLogoutController constructor.
     * @param UserRepositoryInterface $mainRepository
     */
    public function __construct(
        private UserRepositoryInterface $mainRepository,
    ) {}

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        auth()->user()->tokens()->delete();

        return $this->success([], 'Tokens Revoked');
    }
}
