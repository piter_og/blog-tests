<?php

namespace App\Http\Controllers\Api\Authentication;

use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerRegisterRequest;
use App\Repositories\Contracts\CustomerRepositoryInterface;
use App\Traits\ApiResponserTrait;
use Illuminate\Support\Facades\Hash;

class CustomerRegisterController extends Controller
{
    use ApiResponserTrait;

    /**
     * UserRegisterController constructor.
     * @param CustomerRepositoryInterface $mainRepository
     */
    public function __construct(
        private CustomerRepositoryInterface $mainRepository
    ) {}

    public function __invoke(CustomerRegisterRequest $request)
    {
        $payload = $request->validated();
        $payload['password'] = Hash::make($payload['password']);

        $customer = $this->mainRepository->create($payload);

        return $this->success([
            'user' => $customer,
            'token' => $customer->createToken('Customer Token', ['customer:manage'])->plainTextToken
        ], 'Customer created successfully!');
    }

}
