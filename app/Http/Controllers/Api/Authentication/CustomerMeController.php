<?php

namespace App\Http\Controllers\Api\Authentication;

use App\Http\Controllers\Controller;
use App\Http\Resources\CustomerResource;
use App\Repositories\Contracts\CustomerRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Traits\ApiResponserTrait;

class CustomerMeController extends Controller
{
    use ApiResponserTrait;

    /**
     * CustomerMeController constructor.
     * @param $
     */
    public function __construct(
        private CustomerRepositoryInterface $mainRepository,
    ) {}

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return $this->success(new CustomerResource(auth()->user()));
    }
}
