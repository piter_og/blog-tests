<?php

namespace App\Http\Controllers\Api\Authentication;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Traits\ApiResponserTrait;

class UserMeController extends Controller
{
    use ApiResponserTrait;

    /**
     * UserMeController constructor.
     * @param $
     */
    public function __construct(
        private UserRepositoryInterface $mainRepository,
    ) {}

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return $this->success([
            'me' => auth()->user(),
        ]);
    }
}
