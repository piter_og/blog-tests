<?php

namespace App\Http\Controllers\Api\Authentication;

use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerLoginRequest;
use App\Repositories\Contracts\CustomerRepositoryInterface;
use App\Traits\ApiResponserTrait;
use Illuminate\Support\Facades\Hash;

class CustomerLoginController extends Controller
{
    use ApiResponserTrait;

    /**
     * UserLoginController constructor.
     * @param CustomerRepositoryInterface $mainRepository
     */
    public function __construct(
        private CustomerRepositoryInterface $mainRepository,
    ) {}

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(CustomerLoginRequest $request)
    {
        $payload = $request->validated();

        $customer = $this->mainRepository->where('email', $payload['email'])->first();

        if (! $customer || ! Hash::check($payload['password'], $customer->password)) {
            return $this->error('Credentials not match', 401);
        }

        return $this->success([
           'token' => $customer->createToken('Customer Token')->plainTextToken
        ], 'Customer logged successfully');
    }
}
