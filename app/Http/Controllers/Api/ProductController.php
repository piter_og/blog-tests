<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Repositories\Contracts\ProductRepositoryInterface;
use App\Traits\ApiResponserTrait;
use http\Exception;

class ProductController extends Controller
{
    use ApiResponserTrait;

    public function __construct(
        protected ProductRepositoryInterface $mainRepository,
    ) {}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payload = ProductResource::collection($this->mainRepository->all());

        return $this->success($payload);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $payload = $request->validated();

        try {
            $product = $this->mainRepository->create($payload);
        } catch (Exception $exception) {
            abort(500, 'Can\'t create the product.');
        }

        return $this->success(new ProductResource($product), 'Created successfully.', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return $this->success(new ProductResource($this->mainRepository->find($product->id)));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        $payload = $request->validated();

        try {
            $this->mainRepository->update($product->id, $payload);
        } catch (Exception $exception) {
            abort(422, 'Can\'t update the product.');
        }

        $updatedProduct = $this->mainRepository->find($product->id);

        return $this->success(new ProductResource($updatedProduct), 'Product updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if (! auth()->user()->tokenCan('product:manage')
            || ($product->user_id != auth()->user()->id)) {
            abort(403, 'You don\'t have permission to remove this product.');
        }

        try {
            $this->mainRepository->delete($product->id);
        } catch (Exception $exception) {
            abort(422, 'Can\'t remove the product.');
        }

        return $this->success('', 'Product removed successfully.', 204);
    }
}
