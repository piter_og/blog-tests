<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Console\Input\InputArgument;

class RepositoryMakeCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:repository {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new repository and contract';

    /**
     * The type of command
     *
     * @var string
     */
    protected $type = 'Repository';

    /**
     * Execute console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->qualifyClass($this->getNameInput());

        $path = $this->getPath($name.'Repository');

        if ((! $this->hasOption('force') ||
                ! $this->option('force')) &&
            $this->alreadyExists($this->getNameInput())) {
            $this->error($this->type.' already exists!');

            return false;
        }

        $this->makeDirectory($path);

        $this->addRepositoryContractInProvider();

        $this->files->put($path, $this->sortImports($this->buildClass($name)));

        $this->info($this->type.' created successfully.');

        Artisan::call('make:repository-contract '.$this->argument('name').'RepositoryInterface');
    }

    protected function replaceClass($stub, $name)
    {
        $stub = parent::replaceClass($stub, $name);
        return str_replace('Generic', $this->argument('name'), $stub);
    }

    /**
     * Get the stub file
     *
     * @return string
     */
    protected function getStub()
    {
        return  'stubs/repository.stub';
    }

    /**
     * Get default namespace to class
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Repositories';
    }

    /**
     * Get arguments from console command
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the repository.'],
        ];
    }

    protected function addRepositoryContractInProvider()
    {
        $filename = app_path().'/Providers/RepositoryServiceProvider.php';

        $search = 'use App\Repositories\{';
        $replace = $search.$this->argument('name').'Repository,';
        file_put_contents($filename, str_replace($search, $replace, file_get_contents($filename)));

        $search = 'use App\Repositories\Contracts\{';
        $replace = $search.$this->argument('name').'RepositoryInterface,';
        file_put_contents($filename, str_replace($search, $replace, file_get_contents($filename)));

        $search = '    public function register()
    {';

        $replace = $search.'
        $this->app->bind('.$this->argument('name').'RepositoryInterface::class, '.$this->argument('name').'Repository::class);';
        file_put_contents($filename, str_replace($search, $replace, file_get_contents($filename)));
    }
}
