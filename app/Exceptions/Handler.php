<?php

namespace App\Exceptions;

use App\Traits\ApiResponserTrait;
use GrahamCampbell\Exceptions\Displayer\DebugDisplayer;
use GrahamCampbell\Exceptions\ExceptionHandler as ExceptionHandler;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    use ApiResponserTrait;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $exception)
    {
        if ($request->wantsJson()) {
            if ($exception instanceof ValidationException) {
                return $this->error('The given data was invalid.', 422, $exception->validator->getMessageBag());
            }

            if ($exception instanceof ModelNotFoundException) {
                return $this->error('Record not found.', 404);
            }

            if ($exception instanceof AuthenticationException
                || $exception instanceof AuthorizationException) {
                return $this->error($exception->getMessage(), 401);
            }

            if ($exception instanceof HttpException) {
                return $this->error($exception->getMessage(), 401);
            }

            return $this->error($exception->getMessage(), 500, $request->all());
        }

        return parent::render($request, $exception);
    }
}
