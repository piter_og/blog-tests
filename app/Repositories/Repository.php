<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

Abstract class Repository
{
    /**
     * @return mixed
     */
    public function all()
    {
        return $this->model->get();
    }

    /**
     * find a record in database
     * @param int $id
     * @return
     */
    public function find(int $id)
    {
        return $this->model->find($id);
    }

    /**
     * paginate set of results per page
     * @param int $perPage
     * @return
     */
    public function paginate(int $perPage = 15)
    {
        return $this->model->paginate($perPage);
    }

    /**
     * Create a new record in database
     * @param array $attributes
     * @return
     */
    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    public function updateOrCreate(array $find, array $update)
    {
        return $this->model->updateOrCreate($find,$update);
    }

    /**
     * update a record in database
     * @param int $id
     * @param array $attributes
     * @return
     */
    public function update(int $id, array $attributes)
    {
        return $this->model->find($id)->update($attributes);
    }

    /**
     * find a record in database
     * @param int $id
     * @return
     */
    public function findOrFail(int $id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * find a record in database
     * @return
     */
    public function query()
    {
        return $this->model->query();
    }

    /**
     * @param $field
     * @param string $signal
     * @param $value
     * @return mixed
     */
    public function where(string $field, string $signal = '=', $value = null)
    {
        return $this->model->where($field, $signal, $value);
    }

    /**
     * Delete a resource for a given id
     * @param int $id
     * @return
     */
    public function delete(int $id)
    {
        return  $this->model->where('id', $id)->delete();
    }
}
