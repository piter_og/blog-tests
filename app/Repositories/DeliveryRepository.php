<?php

namespace App\Repositories;

use App\Models\Delivery;
use App\Repositories\Contracts\DeliveryRepositoryInterface;

class DeliveryRepository extends Repository implements DeliveryRepositoryInterface
{
    protected $model;

    public function __construct(Delivery $model)
    {
        $this->model = $model;
    }
}
