<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->title,
            'description' => $this->faker->sentence(5),
            'price' => $this->faker->randomFloat(2, 0, 999),
            'quantity' => $this->faker->randomNumber(3),
        ];
    }

    /**
     * @return ProductFactory
     */
    public function definedUser()
    {
        return $this->state(function (array $attributes) {
            return [
                'user_id' => $attributes['user_id']
            ];
        });
    }
}
