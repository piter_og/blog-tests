# TestProject
Project to test Laravel features and packages;

## Context
OrtizShop is an online store API where you can register your products and sell to other people.
Just customers can buy products.


## Roadmap
- [x] Define migrations
- [ ] Define api routes
- [ ] Define api resources
- [x] Stubs
    - [x] Repository
    - [x] Contract
    - [x] Trait
- [x] Add Repository Layer
- [ ] Controllers
    - [x] UserAuth (Register/Login/Me/Logout)
    - [x] CustomerAuth (Register/Login/Me/Logout)
    - [ ] Order
    - [ ] Delivery
- [x] Traits
    - [x] Response API
- [ ] Language
    - [ ] en
    - [ ] pt_BR
- [ ] Tests
    - [x] Products (Feature)
    - [x] Customer (Feature)
    - [ ] Order (Feature)
    - [ ] Delivery (Feature)



4 tables (User / Product / Order / Delivery);

Api Crud With Form Request;



Add Service Layer;

Build tests;


## Technologies and Versions

- PHP8
- Laravel 8
    - Sanctum 2.*
